# React Tech Challenge

Hey all,

This was an interesting challenge.

I spent a little longer than an hour styling it and scrutinizing it.

Some notes:
- My linter is acting up so I apologize for any linting errors -- This means I wasn't linting on save which I normally do.
- Some code is repeated (IE the grey color for the background) - I should have it such that it's written out only once.
- MPCard is inside of PostalForm when they should probably be siblings. It would be better to use some sort of state management library (like redux) or put a function in the parent that controls the MP Info that is displayed.
- A regex to validate the data before sending it to the API would be good for generating better errors. Unfortunately the API doesn't give informative errors.
- PostalForm is a large file and it would do good to break it up
- It would be good to put the API in a redux action as well to decouple the API call from the component(https://redux.js.org/advanced/async-actions#actionsjs-asynchronous)
- Also it would be an improvement if the commits were smaller and reflected one change / feature