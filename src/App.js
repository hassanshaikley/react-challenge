import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import PostalForm from './Components/PostalForm'
import { Box, Text, Grommet } from 'grommet';


class App extends Component {
  render() {
    return (
      <Grommet>
        <Box pad='large' background='dark-2' />
        <Box
          direction='row'
          pad='medium'
        >
          <Text size='xxlarge' color='white' alignSelf='center' style={{ margin: 'auto', textTransform: 'uppercase', fontWeight: 100 }}>Find MP by Postal Code</Text>
        </Box>
        <Box
          direction='row'
          border={{ size: 'large', color: 'dark-2' }}
          pad='medium'
          background='white'
          animation='fadeIn'
        >
          <PostalForm />
        </Box>
      </Grommet>

    );
  }
}

export default App;
