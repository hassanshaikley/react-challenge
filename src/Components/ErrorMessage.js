import React from 'react';

export default (props) => {
  return (<div style={{ color: 'red', textAlign: 'center' }}>{props.message}</div>)
}