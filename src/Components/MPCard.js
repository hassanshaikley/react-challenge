import React from 'react';
import { Box, Text } from 'grommet';

export default (props) => {
    const { email, name, district, imgSource, website } = props;

    return (
        <Box
            animation='fadeIn'
            id='mp-card'
        >
            <Text size='large'>{name}</Text>
            <img src={imgSource} />
            <Text size='medium'>{district}</Text>
            <a href={`mailto:${email}?Subject=Hello%20World`} target='_top'>
                <Text size='small'>
                    {email}
                </Text>
            </a>
            <a href={website}>
                <Text size='small'>
                    {website}
                </Text>
            </a>
        </Box>
    )
}