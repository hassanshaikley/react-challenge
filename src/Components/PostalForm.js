import React from 'react'
import fetchJsonp from 'fetch-jsonp';
import ErrorMessage from './ErrorMessage';
import MPCard from './MPCard'
import { TextInput, Button, Icons, Meter } from 'grommet'
import { FormNext } from 'grommet-icons';


export default class PostalForm extends React.Component {
    state = {
        value: '',
        error: undefined,
        mp: undefined,
        loading: false
    }

    submit = (event) => {
        event.preventDefault();
        this.setState({ loading: true })

        const { value } = this.state
        const whiteSpace = /\s+/g
        const validPostalCode = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/;
        const valueIsValidPostalCode = validPostalCode.test(value)

        if (!valueIsValidPostalCode) {
            return this.setState({
                error: 'Invalid Postal Code',
                loading: false,
                mp: undefined
            })
        }

        fetchJsonp(`https://represent.opennorth.ca/postcodes/${value.replace(whiteSpace, '').toUpperCase()}/?format=jsonp`)
            .then(({ json }) => {
                return json()
            })
            .then(response => {
                const { representatives_centroid } = response
                this.setState({
                    error: undefined,
                    mp: representatives_centroid.find((representative) => representative.elected_office === 'MP'),
                    loading: false
                })
            })
            .catch(error => {
                this.setState({
                    error: 'Request Failed',
                    mp: undefined,
                    loading: false
                })
            })
    }

    handleChange = (event) => {
        this.setState({ value: event.target.value });
    }

    render() {
        const { error, value, mp, loading } = this.state

        return (
            <form onSubmit={this.submit} style={{ margin: 'auto' }}>
                {error && <ErrorMessage message={error} />}
                {mp && <MPCard email={mp.email} name={mp.name} district={mp.district_name} imgSource={mp.photo_url} website={mp.personal_url} />}
                <br />
                <TextInput value={value} onChange={this.handleChange} placeholder='A1A 1A1' autoFocus={true} />
                <br />
                <Button
                    fill={true}
                    label='Go'
                    onClick={this.submit}
                    icon={<FormNext />}
                    style={{ border: '2px solid #999999' }}
                    disabled={loading}
                />
                <br />
            </form>
        )
    }
}